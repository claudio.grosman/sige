import React, { Component } from 'react';
import './LoginComponent.css';
import { Button, Form, FormGroup, Label, Input}
  from 'reactstrap';

class LoginComponent extends Component {
  render() {
    return (  
      <Form className="login-form">
        <h1 className="text-center">
          <span className="font-weight-bold">Central de Estágio</span>
        </h1>
        <FormGroup>
          <Label>RA</Label>
          <Input type="text" placeholder="RA"/>
        </FormGroup>
        <FormGroup>
          <Label>Senha</Label>
          <Input type="password" placeholder="Senha"/>
        </FormGroup>
        <Button className="btn-lg btn-dark btn-block">Acessar</Button>
        <div className="text-center pt-3">
          Problemas no acesso?
        </div>
        <div className="text-center">
          <a href="/help-me">Ajude-me</a>
          <span className="p-2">|</span>
          <a href="/fotgot-passowrd">Esqueci minha senha</a>
        </div>
      </Form>
    );
  }
}

export default LoginComponent;
