## SIGE
### `dependencies`
- Node js (https://nodejs.org/en/download/)
    ``` 
    sudo apt install nodejs
    nodejs -v
    ```
- npm
    ```
    npm --version
    ```
- react
    ```
    sudo npm install create-react-app -g
    ```
- login page
    ```
    mpm i bootstrap reactstrap react-social-login-buttons
    ```
### `start project`
```
npm install
npm start
```
